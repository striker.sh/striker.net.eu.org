#+options: ':t *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline author:nil
#+options: broken-links:nil c:nil creator:nil d:(not "LOGBOOK") date:nil e:t
#+options: email:nil f:t inline:t num:nil p:nil pri:nil prop:nil stat:nil tags:t
#+options: tasks:t tex:t timestamp:nil title:t toc:nil todo:nil |:t
#+TITLE: Striker's /home
#+date: <2021-01-23 Sat>
#+author: striker
#+language: en
#+select_tags: export
#+exclude_tags: noexport
#+options: html-link-use-abs-url:nil html-postamble:auto html-preamble:t
#+options: html-scripts:t html-style:t html5-fancy:nil tex:t
#+html_doctype: xhtml-strict
#+html_container: div
#+description:
#+keywords:
#+html_link_home:
#+html_link_up:
#+html_mathjax:
#+html_equation_reference_format: \eqref{%s}
#+html_head: <link rel="stylesheet" type="text/css" href="css/style.css" />
#+html_head_extra: <link rel="stylesheet" type="text/css" href="css/syntax.css" />
#+subtitle:
#+infojs_opt:
#+creator: <a href="https://www.gnu.org/software/emacs/">Emacs</a> 27.1 (<a href="https://orgmode.org">Org</a> mode 9.5)
#+latex_header:

* whoami

file:img/pp.png

I'm a French computer science student .

I like anime and japanese pop culture in general.

And I am interrested in OpenSource and Homebrew communities.

| Languages          | French (Native), English, a bit of German and Japanese |
| Programming skills | C, C++, Java, Python, Web Stuff                        |
| Editors            | Vim, Emacs                                             |
| OS                 | GNU/Linux                                              |

* Hosted on this domain
This domain name is mostly used for selfhosting stuff:
- [[https://pleroma.striker.net.eu.org/striker][Pleroma]] : a federated microbloging platform based on fediverse
- [[https://searx.striker.net.eu.org/][Searx]] : a meta search engine
- [[https://live.striker.net.eu.org][Live streaming]] : For streaming video games through rtmp
- syncplay.striker.net.eu.org : Synchronize your media player with friends
- chat.striker.net.eu.org : An XMPP server

* VCS
- [[https://gitlab.com/striker.sh/][Gitlab]]
- [[https://github.com/strikersh/][Github]]

* Social
- [[https://pleroma.striker.net.eu.org/striker][Pleroma]]
- [[https://matrix.to/#/@strikersh:matrix.org][Matrix]]
- [[https://anilist.co/user/strikersan][Anilist]]
- @@html:<a href="xmpp:striker@striker.net.eu.org">@@XMPP@@html:</a>@@
- IRC : strikersh on freenode

* Misc
** Syntax hilighting
  #+begin_src c
void Il1egal0O(int null){
    switch(1|I[i]){
        case check_tab break;
    }
    return false;
}
  #+end_src
